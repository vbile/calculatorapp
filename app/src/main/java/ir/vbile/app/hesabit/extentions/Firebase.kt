package ir.vbile.app.hesabit.extentions

import androidx.core.os.bundleOf
import com.google.firebase.analytics.FirebaseAnalytics

fun FirebaseAnalytics.logViewFactor(id: String) = logEvent(
    FirebaseAnalytics.Event.VIEW_ITEM,
    bundleOf(
        FirebaseAnalytics.Param.CONTENT_TYPE to "view_factor",
        FirebaseAnalytics.Param.ITEM_ID to id
    )
)
fun FirebaseAnalytics.logCreateFactor(id: String) = logEvent(
    FirebaseAnalytics.Event.VIEW_ITEM,
    bundleOf(
        FirebaseAnalytics.Param.CONTENT_TYPE to "view_factor",
        FirebaseAnalytics.Param.ITEM_ID to id
    )
)
fun FirebaseAnalytics.logEditProduct(id: String) = logEvent(
    FirebaseAnalytics.Event.VIEW_ITEM,
    bundleOf(
        FirebaseAnalytics.Param.CONTENT_TYPE to "edit_product",
        FirebaseAnalytics.Param.ITEM_ID to id
    )
)
fun FirebaseAnalytics.logCreateProduct(id: String) = logEvent(
    FirebaseAnalytics.Event.VIEW_ITEM,
    bundleOf(
        FirebaseAnalytics.Param.CONTENT_TYPE to "create_product",
        FirebaseAnalytics.Param.ITEM_ID to id
    )
)
fun FirebaseAnalytics.logCreateCategory(id: String) = logEvent(
    FirebaseAnalytics.Event.VIEW_ITEM,
    bundleOf(
        FirebaseAnalytics.Param.CONTENT_TYPE to "create_category",
        FirebaseAnalytics.Param.ITEM_ID to id
    )
)
