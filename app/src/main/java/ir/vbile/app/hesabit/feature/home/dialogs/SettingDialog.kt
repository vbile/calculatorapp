package ir.vbile.app.hesabit.feature.home.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.databinding.DialogSettingsBinding

class SettingDialog : BottomSheetDialogFragment() {
    lateinit var binding: DialogSettingsBinding
    var settingDialogEventListener : SettingDialogEventListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogSettingsBinding.bind(view)
        binding.btnCreateCategory.setOnClickListener {
            settingDialogEventListener?.onCreateCategoryClick()
        }
        binding.btnCreateProduct.setOnClickListener {
            settingDialogEventListener?.onCreateProductClick()
        }
    }


    interface SettingDialogEventListener{
        fun onCreateCategoryClick()
        fun onCreateProductClick()
    }

    override fun getTheme(): Int = R.style.AppBottomSheetDialogTheme
}