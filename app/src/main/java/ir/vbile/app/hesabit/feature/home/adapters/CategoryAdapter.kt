package ir.vbile.app.hesabit.feature.home.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.extentions.implementSpringAnimationTrait
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter : ListAdapter<Category, BaseViewHolder>(CallBack()) {

    var categoryClickListener: OnCategoryClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    private inner class CategoryViewHolder(override val containerView: View) : LayoutContainer,
        BaseViewHolder(containerView.rootView) {
        override fun bind(position: Int) {
            containerView.rootView.apply {
                val category = getItem(position) ?: return
                implementSpringAnimationTrait()
                setOnClickListener {
                    categoryClickListener?.onCategoryClick(category)
                }
                tvName.text = category.name
                tvNumber.text = category.id.toString()
            }
        }
    }

    companion object {
        class CallBack : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }
    interface OnCategoryClickListener {
        fun onCategoryClick(category: Category)
    }
}