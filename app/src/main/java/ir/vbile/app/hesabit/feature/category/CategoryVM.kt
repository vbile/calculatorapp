package ir.vbile.app.hesabit.feature.category

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.category.CategoryDao
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryVM @Inject constructor(
    private val categoryDao: CategoryDao
) : BaseVM() {
    private val categoriesFlow = categoryDao.getCategories()
    val categories = categoriesFlow.asLiveData()

    private val categoryEventChannel = Channel<CategoryEvent>()
    val categoryEvent = categoryEventChannel.receiveAsFlow()

    sealed class CategoryEvent {
        object ShowCreateCategoryDialog : CategoryEvent()
        data class ShowEditCategoryDialog(val category: Category?) : CategoryEvent()
        data class ShowInvalidInputMessage(val msg: String) : CategoryEvent()
        data class ShowUndoDeleteCategoryMessage(val category: Category) : CategoryEvent()
        object ShowCategorySavedConfirmationMessage : CategoryEvent()
        object ShowCategoryUpdatedMessage : CategoryEvent()
    }

    fun onCreateCategory() = viewModelScope.launch {
        categoryEventChannel.send(CategoryEvent.ShowCreateCategoryDialog)
    }
    fun onEditCategory(category: Category?) = viewModelScope.launch {
        categoryEventChannel.send(CategoryEvent.ShowEditCategoryDialog(category))
    }

    fun createCategory(categoryName: String) = viewModelScope.launch {
        if (categoryName.isEmpty()) {
            categoryEventChannel.send(CategoryEvent.ShowInvalidInputMessage("لطفاً مقدار درستی وارد کنید"))
            return@launch
        }
        categoryDao.insert(Category(categoryName))
        categoryEventChannel.send(CategoryEvent.ShowCategorySavedConfirmationMessage)
    }

    fun updateCategory(category: Category) = viewModelScope.launch {
        categoryDao.update(category)
        // Dismiss Dialog
        categoryEventChannel.send(CategoryEvent.ShowCategoryUpdatedMessage)
    }

    fun onCategorySwiped(category: Category) = viewModelScope.launch {
        categoryDao.delete(category)
        categoryEventChannel.send(CategoryEvent.ShowUndoDeleteCategoryMessage(category))
    }

    fun undoDeleteClick(category: Category) = viewModelScope.launch {
        categoryDao.insert(category)
    }
}