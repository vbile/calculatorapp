package ir.vbile.app.hesabit.feature.factor.addfactor

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.factor.FactorDao
import ir.vbile.app.hesabit.data.factor.item.FactorItem
import ir.vbile.app.hesabit.data.factor.item.FactorItemDao
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.data.product.ProductDao
import ir.vbile.app.hesabit.data.product.ProductUIModel
import ir.vbile.app.hesabit.other.Constants
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEditFactorVM @Inject constructor(
    val state: SavedStateHandle,
    val factorDao: FactorDao,
    val factorItemDao: FactorItemDao,
    val productDao: ProductDao,
) : BaseVM() {
    val customerName = state.get<String>("customerName") ?: "فروش روزانه"
    val categoryId = state.get<Long>("categoryId") ?: 1
    var factorId = state.get<Long>("factorId") ?: -1L
    val isFirstShowing: MutableLiveData<Boolean> = MutableLiveData()
    private val factorItemsFlow = factorItemDao.factorWithItems(factorId)
    val factorItems = factorItemsFlow.asLiveData()

    val productsFlow = productDao.getProductsWithCategoryId(categoryId)
    val productUIModels: MutableLiveData<List<ProductUIModel>> = MutableLiveData()
    fun createFactorItems(products: List<Product>) = viewModelScope.launch {
        products.map { product ->
            val factorItem = FactorItem(
                product = product, 12, factorId
            )
            factorItemDao.insert(factorItem)
        }
    }

    var isAllProductsSelected: MutableLiveData<Boolean> = MutableLiveData()
    fun addProduct(product: ProductUIModel) = viewModelScope.launch {
        val newValue = productUIModels.value?.map {
            when (it) {
                product -> it.copy(selected = true)
                else -> it
            }
        }
        var isAllProductsSelected = true
        newValue?.map {
            if (!it.selected) isAllProductsSelected = false
        }
        this@AddEditFactorVM.isAllProductsSelected.postValue(isAllProductsSelected)
        productUIModels.postValue(newValue)
    }

    fun removeProduct(product: ProductUIModel) = viewModelScope.launch {
        val newValue = productUIModels.value?.map {
            when (it) {
                product -> it.copy(selected = false)
                else -> it
            }
        }
        var isAllProductsSelected = true
        newValue?.map {
            if (!it.selected) isAllProductsSelected = false
        }
        this@AddEditFactorVM.isAllProductsSelected.postValue(isAllProductsSelected)
        productUIModels.postValue(newValue)
    }

    fun selectProducts() = viewModelScope.launch {
        val newValue = productUIModels.value?.map {
            val factorItem = FactorItem(product = it.toModel(), factorId = factorId)
            factorItemDao.insert(factorItem)
            it.copy(selected = true)
        }
        productUIModels.postValue(newValue)
    }

    fun deselectProducts() = viewModelScope.launch {
        val newValue = productUIModels.value?.map {
            factorItemDao.deleteByProductName(it.name,factorId)
            it.copy(selected = false)
        }
        productUIModels.postValue(newValue)
    }

    fun increaseProductCount(factorItem: FactorItem) = viewModelScope.launch {
        factorItem.count++
        factorItem.totalPrice = factorItem.count * factorItem.product.price
        factorItemDao.update(factorItem)
    }

    fun decreaseProductCount(factorItem: FactorItem) = viewModelScope.launch {
        if (factorItem.count == 0) {
            return@launch
        }
        factorItem.count--
        factorItem.totalPrice = factorItem.count * factorItem.product.price
        factorItemDao.update(factorItem)
    }

    fun onUpdateFactorItem(factorItem: FactorItem) = viewModelScope.launch {
        productDao.update(factorItem.product)
        factorItemDao.update(factorItem)
    }

    fun addProductsToFactor() = viewModelScope.launch {
        productUIModels.value?.map { productUIModel ->
            if (productUIModel.selected) {
                val factorItem = FactorItem(
                    product = productUIModel.toModel(),
                    count = Constants.PRODUCT_DEFAULT_COUNT,
                    factorId = factorId
                )
                factorItemDao.insert(factorItem)
            } else {
                val factorItem = FactorItem(
                    product = productUIModel.toModel(),
                    count = Constants.PRODUCT_DEFAULT_COUNT,
                    factorId = factorId
                )
                factorItemDao.update(factorItem)
            }
        }
    }

    fun syncProductsWithFactorItems() {
        productUIModels.value?.map { productUIModel ->
            factorItems.value?.map { factorItem ->
                productUIModel.selected = productUIModel.name == factorItem.product.name
            }
        }
    }

    fun addFactorItem(product: Product) = viewModelScope.launch {
        val factorItem = FactorItem(
            product = product,
            count = Constants.PRODUCT_DEFAULT_COUNT,
            factorId = factorId
        )
        factorItemDao.insert(factorItem)
    }

    fun deleteFactorItem(product: ProductUIModel) = viewModelScope.launch {
        factorItemDao.deleteByProductName(product.name,factorId)
    }

}