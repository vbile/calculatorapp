package ir.vbile.app.hesabit.feature.calculator

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.extentions.english2Persian
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_calculator.view.*
import kotlinx.android.synthetic.main.item_category.view.tvName

class CalculatorAdapter : ListAdapter<Product, BaseViewHolder>(CallBack()) {

    var productClickListener: OnProductClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_calculator, parent, false)
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    private inner class CategoryViewHolder(override val containerView: View) : LayoutContainer,
        BaseViewHolder(containerView.rootView) {
        override fun bind(position: Int) {
            containerView.rootView.apply {
                val product = getItem(position) ?: return
                setOnClickListener {
                    productClickListener?.onProductClick(product)
                }
                btnAdd.setOnClickListener {
                    productClickListener?.onIncreaseClick(product, 0)
                }
                btnRemove.setOnClickListener {
                    productClickListener?.onDecreaseClick(product, 1)
                }
                tvNumber.text = (position + 1).toString().english2Persian()
                tvName.text = product.name
                tvCount.text = 0.toString().english2Persian()
                etFee.setText(product.price.toInt().toString().english2Persian())
                tvPrice.text = context.getString(
                    R.string.item_calculator_price,
                    product.price.toInt()
                )
            }
        }
    }

    companion object {
        class CallBack : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }

    interface OnProductClickListener {
        fun onProductClick(product: Product)
        fun onIncreaseClick(product: Product, count: Int)
        fun onDecreaseClick(product: Product, count: Int)
    }
}