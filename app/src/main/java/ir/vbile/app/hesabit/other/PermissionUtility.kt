package ir.vbile.app.hesabit.other

import android.Manifest
import android.content.Context
import pub.devrel.easypermissions.EasyPermissions

object PermissionUtility {
    fun hasWriteExternalStoragePermissions(context: Context) =
        EasyPermissions.hasPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
}