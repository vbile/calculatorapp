package ir.vbile.app.hesabit.feature.calculator

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.data.product.ProductDao
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CalculatorVM @Inject constructor(
    state: SavedStateHandle,
    productDao: ProductDao
) : BaseVM() {
    private val categoryId = state.getLiveData<Category>("category").value?.id ?: -1
    private val productEventChannel = Channel<ProductEvent>()
    private val productFlow = productDao.getProductsWithCategoryId(categoryId)
    val productEvent = productEventChannel.receiveAsFlow()
    val products = productFlow.asLiveData()


    fun increaseProductCount(product: Product, count: Int) = viewModelScope.launch {
//        factorDao.update(factor)
    }

    fun decreaseProductCount(product: Product, count: Int) = viewModelScope.launch {

    }

    sealed class ProductEvent {

    }
}