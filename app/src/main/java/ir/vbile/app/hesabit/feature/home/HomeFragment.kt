package ir.vbile.app.hesabit.feature.home

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.databinding.FragmentHomeBinding
import ir.vbile.app.hesabit.extentions.getGreetingMessage
import ir.vbile.app.hesabit.extentions.logCreateCategory
import ir.vbile.app.hesabit.extentions.setVisibleOrGone
import ir.vbile.app.hesabit.feature.home.adapters.CategoryAdapter
import ir.vbile.app.hesabit.feature.home.dialogs.CreateEditCategoryDialog
import ir.vbile.app.hesabit.feature.home.dialogs.CreateEditProductDialog
import ir.vbile.app.hesabit.feature.home.dialogs.SettingDialog
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeVM>(R.layout.fragment_home, HomeVM::class),
    CategoryAdapter.OnCategoryClickListener,
    SettingDialog.SettingDialogEventListener,
    CreateEditCategoryDialog.CreateCategoryEventListener,
    CreateEditProductDialog.CreateProductEventListener {

    lateinit var categoryAdapter: CategoryAdapter
    private lateinit var binding: FragmentHomeBinding
    var settingDialog: SettingDialog? = null
    var createEditCategoryDialog: CreateEditCategoryDialog? = null
    var createEditProductDialog: CreateEditProductDialog? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews(view)
        setupSettingsAdapter()
        setupCreateCategoryDialog()
        setUpCategoryAdapter()
        setUpCreateProductDialog()
        subscribeToObservers()
    }

    private fun setUpCreateProductDialog() {
        vm.categories.observe(viewLifecycleOwner) {
            createEditProductDialog = CreateEditProductDialog(it)
            createEditProductDialog?.createProductEventListener = this
        }
    }

    private fun subscribeToObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.uiState.collectLatest { uiState ->
                when (uiState) {
                    is BaseVM.UiState.Loading -> {
                        setProgressIndicator(uiState.shouldShow)
                    }
                    is BaseVM.UiState.EmptyState -> {
                        emptyState.setVisibleOrGone(uiState.shouldShow)
                    }
                }
            }
        }
        vm.categories.observe(viewLifecycleOwner) {
            categoryAdapter.submitList(it)
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.categoryEvent.collect { event ->
                when (event) {
                    is HomeVM.CategoryEvent.NavigateToCreateFactorScreen -> {
                        val action =
                            HomeFragmentDirections.actionHomeFragmentToFactorFragment(event.category)
                        findNavController().navigate(action)
                    }
                    HomeVM.CategoryEvent.ShowCreateCategoryDialog -> {
                        createEditCategoryDialog?.show(childFragmentManager, null)
                    }
                    HomeVM.CategoryEvent.ShowCreateProductDialog -> {
                        createEditProductDialog?.show(childFragmentManager, null)
                    }
                    is HomeVM.CategoryEvent.ShowInvalidInputMessage -> {
                        toast(event.msg)
                    }
                    is HomeVM.CategoryEvent.NavigateBackWithResult -> {
                        createEditCategoryDialog?.dismiss()
                        toast(event.message)
                        rvCategories.smoothScrollToPosition(categoryAdapter.currentList.size)
                        firebaseAnalytics.logCreateCategory(event.message)
                    }
                }
            }
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.productEvent.collect { event ->
                when (event) {
                    is HomeVM.ProductEvent.NavigateBackWithResult -> {
                        createEditProductDialog?.dismiss()
                        toast(event.message)
                    }
                }
            }
        }
    }

    private fun setUpCategoryAdapter() {
        categoryAdapter = CategoryAdapter()
        binding.rvCategories.apply {
            adapter = categoryAdapter
        }
        categoryAdapter.categoryClickListener = this

    }

    private fun setUpViews(view: View) {
        binding = FragmentHomeBinding.bind(view)
        binding.tvGreeting.text = getGreetingMessage()
        binding.btnMoreSetting.setOnClickListener {
            settingDialog?.show(childFragmentManager, null)
        }
        binding.btnCreateCategory.setOnClickListener {
            createEditCategoryDialog?.show(childFragmentManager, null)
        }
    }

    private fun setupCreateCategoryDialog() {
        createEditCategoryDialog = CreateEditCategoryDialog()
        createEditCategoryDialog?.createCategoryEventListener = this
    }

    private fun setupSettingsAdapter() {
        settingDialog = SettingDialog()
        settingDialog?.settingDialogEventListener = this
    }

    override fun onCategoryClick(category: Category) {
        vm.onCategoryClicked(category)
    }

    override fun onCreateCategoryClick() {
        vm.onCreateCategoryClick()
    }

    override fun onCreateProductClick() {
        vm.onCreateProductClick()
    }

    override fun createCategory(categoryName: String) {
        if (settingDialog?.isVisible == true) {
            settingDialog?.dismiss()
        }
        vm.createCategory(categoryName)
    }

    override fun saveProduct(category: Category, productName: String, price: Float) {
        settingDialog?.dismiss()
        vm.createProduct(category, productName, price)
    }

    override fun updateCategory(category: Category) {

    }

    override fun updateProduct(product: Product) = Unit
}