package ir.vbile.app.hesabit.feature.home

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.category.CategoryDao
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.data.product.ProductDao
import ir.vbile.app.hesabit.feature.main.ADD_CATEGORY_RESULT_OK
import ir.vbile.app.hesabit.feature.main.ADD_RESULT_OK
import ir.vbile.app.hesabit.feature.main.EDIT_CATEGORY_RESULT_OK
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeVM @Inject constructor(
    val categoryDao: CategoryDao,
    val productDao: ProductDao
) : BaseVM() {
    private val categoryEventChannel = Channel<CategoryEvent>()
    private val productEventChannel = Channel<ProductEvent>()
    val categoryEvent = categoryEventChannel.receiveAsFlow()
    val productEvent = productEventChannel.receiveAsFlow()
    private val categoryFlow = categoryDao.getCategories().apply {
        viewModelScope.launch {
            collectLatest {
                if (it.isNotEmpty()) {
                    _uiState.emit(UiState.Loading(false))
                    _uiState.emit(UiState.EmptyState(false))
                } else {
                    _uiState.emit(UiState.EmptyState(true))
                    _uiState.emit(UiState.Loading(false))
                }
            }
        }
    }
    val categories = categoryFlow.asLiveData()


    fun onCategoryClicked(category: Category) = viewModelScope.launch {
        categoryEventChannel.send(CategoryEvent.NavigateToCreateFactorScreen(category))
    }

    fun onCreateCategoryClick() = viewModelScope.launch {
        categoryEventChannel.send(CategoryEvent.ShowCreateCategoryDialog)
    }

    fun onCreateProductClick() = viewModelScope.launch {
        categoryEventChannel.send(CategoryEvent.ShowCreateProductDialog)
    }

    fun createCategory(categoryName: String) = viewModelScope.launch {
        if (categoryName.isEmpty()) {
            categoryEventChannel.send(CategoryEvent.ShowInvalidInputMessage("لطفاً مقدار درستی وارد کنید"))
            return@launch
        }
        val category = Category(categoryName)
        categoryDao.insert(category)
        // Dismiss Dialog
        categoryEventChannel.send(
            CategoryEvent.NavigateBackWithResult(
                "با موفقیت انجام شد",
                ADD_CATEGORY_RESULT_OK,
                category
            )
        )
    }

    fun updateCategory(category: Category) = viewModelScope.launch {
        categoryDao.update(category)
        // Dismiss Dialog
        categoryEventChannel.send(
            CategoryEvent.NavigateBackWithResult(
                "با موفقیت انجام شد",
                EDIT_CATEGORY_RESULT_OK,
                category
            )
        )
    }

    fun createProduct(category: Category, productName: String, price: Float) =
        viewModelScope.launch {
            val product = Product(productName, price, categoryId = category.id)
            productDao.insert(product)
            // Dismiss Dialog
            productEventChannel.send(
                ProductEvent.NavigateBackWithResult(
                    "با موفقیت انجام شد",
                    ADD_RESULT_OK
                )
            )
        }

    sealed class CategoryEvent {
        object ShowCreateCategoryDialog : CategoryEvent()
        object ShowCreateProductDialog : CategoryEvent()
        data class NavigateToCreateFactorScreen(val category: Category) : CategoryEvent()
        data class ShowInvalidInputMessage(val msg: String) : CategoryEvent()
        data class NavigateBackWithResult(val message: String, val result: Int,val category: Category) : CategoryEvent()
    }

    sealed class ProductEvent {
        data class NavigateBackWithResult(val message: String, val result: Int) : ProductEvent()
    }
}