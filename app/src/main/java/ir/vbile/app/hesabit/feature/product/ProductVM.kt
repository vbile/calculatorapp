package ir.vbile.app.hesabit.feature.product

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.category.CategoryDao
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.data.product.ProductDao
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductVM @Inject constructor(
    val productDao: ProductDao,
    val categoryDao: CategoryDao
) : BaseVM() {
    private val productsFlow = productDao.getProducts()
    val products = productsFlow.asLiveData()

    private val categoriesFlow = categoryDao.getCategories()
    val categories = categoriesFlow.asLiveData()

    sealed class ProductEvent {
        data class ShowProductEditDialog(val product: Product, val categories: List<Category>) :
            ProductEvent()

        object ShowProductSuccessfullyUpdatesAndDismissDialog : ProductEvent()
        data class ShowUndoDeleteProductMessage(val product: Product) : ProductEvent()
        object ShowProductSavedConfirmationMessage : ProductEvent()
    }

    private val productEventChannel = Channel<ProductEvent>()
    val productEvents = productEventChannel.receiveAsFlow()

    fun onProductClicked(product: Product) = viewModelScope.launch {
        productEventChannel.send(
            ProductEvent.ShowProductEditDialog(product, categories.value!!)
        )
    }

    fun updateProduct(product: Product) = viewModelScope.launch {
        productDao.update(product)
        productEventChannel.send(ProductEvent.ShowProductSuccessfullyUpdatesAndDismissDialog)
    }

    fun onProductSwiped(product: Product) = viewModelScope.launch {
        productDao.delete(product)
        productEventChannel.send(ProductEvent.ShowUndoDeleteProductMessage(product))
    }

    fun undoDeleteClick(product: Product) = viewModelScope.launch {
        productDao.insert(product)
    }

    fun createProduct(category: Category, productName: String, price: Float) =
        viewModelScope.launch {
            val product = Product(productName, price, categoryId = category.id)
            productDao.insert(product)
            // Dismiss Dialog
            productEventChannel.send(ProductEvent.ShowProductSavedConfirmationMessage)
        }
}
