package ir.vbile.app.hesabit.feature.factor.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.databinding.DialogCreateFactorBinding
import ir.vbile.app.hesabit.extentions.afterTextChanged

class CreateFactorDialog : BottomSheetDialogFragment() {
    lateinit var binding: DialogCreateFactorBinding
    var createDialogListener: CreateDialogListener? = null
    var name = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_create_factor, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogCreateFactorBinding.bind(view)
        binding.etCustomerName.afterTextChanged {
            name = it
        }
        binding.btnCreateFactor.setOnClickListener {
            if (name.isEmpty()) {
                Toast.makeText(requireContext(), "لطفاً نام مشتری را وارد کنید", Toast.LENGTH_SHORT)
                    .show()
            } else
                createDialogListener?.createDialog(name)
        }
    }

    override fun getTheme(): Int = R.style.AppBottomSheetDialogTheme

    interface CreateDialogListener {
        fun createDialog(customerName: String)
    }
}