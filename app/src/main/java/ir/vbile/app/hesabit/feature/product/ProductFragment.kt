package ir.vbile.app.hesabit.feature.product

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.databinding.FragmentProductBinding
import ir.vbile.app.hesabit.feature.home.dialogs.CreateEditProductDialog
import kotlinx.android.synthetic.main.fragment_product.*
import kotlinx.coroutines.flow.collect
import timber.log.Timber

@AndroidEntryPoint
class ProductFragment : BaseFragment<ProductVM>(R.layout.fragment_product, ProductVM::class),
    ProductsAdapter.ProductsEventListener, CreateEditProductDialog.CreateProductEventListener {
    private lateinit var binding: FragmentProductBinding
    private var productsAdapter = ProductsAdapter()
    var createEditProductDialog: CreateEditProductDialog? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        productsAdapter.setOnProductEventListener(this)
        subscribeToObservers()
    }
    private fun setUpViews() {
        binding = FragmentProductBinding.bind(requireView())
        binding.rvProducts.apply {
            adapter = productsAdapter
        }
        binding.btnCreateProduct.setOnClickListener {
            createEditProductDialog = CreateEditProductDialog(vm.categories.value!!)
            createEditProductDialog?.createProductEventListener = this
            createEditProductDialog?.show(childFragmentManager, null)
        }
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val product = productsAdapter.currentList[viewHolder.bindingAdapterPosition]
                vm.onProductSwiped(product)
            }
        }).attachToRecyclerView(binding.rvProducts)
    }

    private fun subscribeToObservers() {
        vm.products.observe(viewLifecycleOwner) {
            Timber.i("${it.size}")
            productsAdapter.submitList(it)
        }
        vm.categories.observe(viewLifecycleOwner) {
            createEditProductDialog = CreateEditProductDialog(it)
            createEditProductDialog?.createProductEventListener = this
        }
        lifecycleScope.launchWhenStarted {
            vm.productEvents.collect { event ->
                when (event) {
                    is ProductVM.ProductEvent.ShowProductEditDialog -> {
                        showProductEditDialog(event.product, event.categories)
                    }
                    ProductVM.ProductEvent.ShowProductSuccessfullyUpdatesAndDismissDialog -> {
                        createEditProductDialog?.dismiss()
                        showSnackBar(getString(R.string.product_successfully_updated))
                    }
                    is ProductVM.ProductEvent.ShowUndoDeleteProductMessage -> {
                        Snackbar.make(
                            requireView(),
                            getString(R.string.product_deleted),
                            Snackbar.LENGTH_LONG
                        )
                            .setAction("انصراف") {
                                vm.undoDeleteClick(event.product)
                            }.show()
                    }
                    ProductVM.ProductEvent.ShowProductSavedConfirmationMessage -> {
                        createEditProductDialog?.dismiss()
                        Snackbar.make(requireView(), getString(R.string.product_successfully_created), Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun showProductEditDialog(product: Product, categories: List<Category>) {
        createEditProductDialog = CreateEditProductDialog(categories = categories, product = product)
        createEditProductDialog?.createProductEventListener = this
        createEditProductDialog?.show(childFragmentManager, null)
    }

    override fun onProductClicked(product: Product) {
        vm.onProductClicked(product)
    }

    override fun saveProduct(category: Category, productName: String, price: Float) {
        vm.createProduct(category, productName, price)
    }

    override fun updateProduct(product: Product) {
        vm.updateProduct(product)
    }

}