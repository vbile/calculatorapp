package ir.vbile.app.hesabit.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.vbile.app.hesabit.feature.calculator.CalculatorAdapter
import ir.vbile.app.hesabit.feature.factor.adapters.FactorAdapter
import ir.vbile.app.hesabit.feature.home.adapters.CategoryAdapter
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AdapterModule {
    @Provides
    @Singleton
    fun provideCategoryAdapter() = CategoryAdapter()

    @Provides
    @Singleton
    fun provideFactorAdapter() = FactorAdapter()

    @Provides
    @Singleton
    fun provideCalculatorAdapter() = CalculatorAdapter()
}