package ir.vbile.app.hesabit.core

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

abstract class BaseVM : ViewModel() {
    protected var _uiState: MutableStateFlow<UiState> = MutableStateFlow(UiState.Loading(true))
    var uiState: StateFlow<UiState> = _uiState

    sealed class UiState {
        data class Loading(var shouldShow: Boolean) : UiState()
        data class EmptyState(var shouldShow: Boolean) : UiState()
    }
}