package ir.vbile.app.hesabit.feature.main

import android.app.Activity
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseActivity
import ir.vbile.app.hesabit.core.setupWithNavController
import ir.vbile.app.hesabit.databinding.ActivityMainBinding
import ir.vbile.app.hesabit.extentions.margin
import ir.vbile.app.hesabit.extentions.setVisibleOrGone
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private var currentNavController: LiveData<NavController>? = null
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        } // Else, need to wait for onRestoreInstanceState
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationMain)

        val navGraphIds = listOf(
            R.navigation.home,
            R.navigation.product,
            R.navigation.category,
            R.navigation.setting
        )

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = bottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.navHostContainer,
            intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, { navController ->
            setupToolbar(navController)
            //      setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }

    private fun setupToolbar(navController: NavController?) {
        navController?.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homeFragment,
                R.id.productFragment,
                R.id.categoryFragment,
                R.id.settingFragment -> {
                    bottomNavigationMain.setVisibleOrGone(true)
                    navHostContainer.margin(bottom = 56f)
                }
                else -> {
                    bottomNavigationMain.setVisibleOrGone(false)
                    navHostContainer.margin(bottom = 0f)
                }
            }
        }
    }
}

// Factor
const val ADD_FACTOR_RESULT_OK = Activity.RESULT_FIRST_USER
const val EDIT_FACTOR_RESULT_OK = Activity.RESULT_FIRST_USER + 1


// Category
const val ADD_CATEGORY_RESULT_OK = Activity.RESULT_FIRST_USER
const val EDIT_CATEGORY_RESULT_OK = Activity.RESULT_FIRST_USER + 1


// Category
const val ADD_RESULT_OK = Activity.RESULT_FIRST_USER
const val EDIT_RESULT_OK = Activity.RESULT_FIRST_USER + 1