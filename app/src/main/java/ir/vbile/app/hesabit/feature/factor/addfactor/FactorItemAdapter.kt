package ir.vbile.app.hesabit.feature.factor.addfactor

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.factor.item.FactorItem
import ir.vbile.app.hesabit.databinding.ItemFactorItemBinding
import ir.vbile.app.hesabit.extentions.english2Persian
import ir.vbile.app.hesabit.extentions.formatPrice
import ir.vbile.app.hesabit.extentions.implementSpringAnimationTrait

class FactorItemAdapter constructor(
    val factorClickListener: OnFactorItemClickListener? = null
) : ListAdapter<FactorItem, BaseViewHolder>(CallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding =
            ItemFactorItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FactorItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    private inner class FactorItemViewHolder(private val binding: ItemFactorItemBinding) :
        BaseViewHolder(binding.root) {
        var isChangedByUser = false

        init {
            binding.root.implementSpringAnimationTrait()
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val factorItem = getItem(position)
                    factorClickListener?.onFactorItemClick(factorItem)
                }
            }
            binding.btnAdd.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val factorItem = getItem(position)
                    factorClickListener?.onIncreaseProductCount(factorItem)
                }
            }
            binding.btnRemove.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val factorItem = getItem(position)
                    factorClickListener?.onDecreaseProductCount(factorItem)
                }
            }
        }

        override fun bind(position: Int) {
            val factorItem = getItem(position) ?: return
            binding.apply {
                tvName.text = factorItem.product.name
                tvCount.text = factorItem.count.toString().english2Persian()
                tvFee.text = itemView.context.getString(
                    R.string.item_factor_item_fee,
                    formatPrice(factorItem.product.price)
                )
                tvPrice.text = itemView.context.getString(
                    R.string.total_price_value,
                    formatPrice(factorItem.totalPrice)
                )
                isChangedByUser = true
            }
        }
    }

    companion object {
        class CallBack : DiffUtil.ItemCallback<FactorItem>() {
            override fun areItemsTheSame(oldItem: FactorItem, newItem: FactorItem): Boolean {
                return oldItem.product.name == newItem.product.name
            }

            override fun areContentsTheSame(oldItem: FactorItem, newItem: FactorItem): Boolean {
                return oldItem == newItem
            }

        }
    }

    interface OnFactorItemClickListener {
        fun onFactorItemClick(factorItem: FactorItem)
        fun onIncreaseProductCount(factorItem: FactorItem)
        fun onDecreaseProductCount(factorItem: FactorItem)
        fun onProductCountChanged(factorItem: FactorItem, newCount: Int?)
        fun onProductPriceChanged(factorItem: FactorItem, price: Int?)
    }
}