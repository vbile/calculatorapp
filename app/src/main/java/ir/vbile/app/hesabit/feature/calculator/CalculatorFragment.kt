package ir.vbile.app.hesabit.feature.calculator

import android.os.Bundle
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.databinding.FragmentCalculatorBinding
import javax.inject.Inject

@AndroidEntryPoint
class CalculatorFragment :
    BaseFragment<CalculatorVM>(R.layout.fragment_calculator, CalculatorVM::class),
    CalculatorAdapter.OnProductClickListener {
    @Inject
    lateinit var calculatorAdapter: CalculatorAdapter
    lateinit var binding: FragmentCalculatorBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCalculatorBinding.bind(view)
        binding.rvProducts.apply {
            adapter = calculatorAdapter
        }
        vm.products.observe(viewLifecycleOwner) {
            calculatorAdapter.submitList(it.products)
        }
    }

    override fun onProductClick(product: Product) {

    }

    override fun onIncreaseClick(product: Product, count: Int) {
        vm.increaseProductCount(product, count)
    }

    override fun onDecreaseClick(product: Product, count: Int) {
        vm.decreaseProductCount(product, count)
    }
}