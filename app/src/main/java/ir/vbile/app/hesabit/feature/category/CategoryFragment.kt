package ir.vbile.app.hesabit.feature.category

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.databinding.FragmentCategoryBinding
import ir.vbile.app.hesabit.feature.home.adapters.CategoryAdapter
import ir.vbile.app.hesabit.feature.home.dialogs.CreateEditCategoryDialog
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_product.*
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class CategoryFragment : BaseFragment<CategoryVM>(R.layout.fragment_category, CategoryVM::class),
    CategoryAdapter.OnCategoryClickListener, CreateEditCategoryDialog.CreateCategoryEventListener {
    private lateinit var binding: FragmentCategoryBinding
    var createEditCategoryDialog: CreateEditCategoryDialog? = null
    private var categoryAdapter = CategoryAdapter()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCategoryBinding.bind(view)
        subscribeToObservers()
        setUpCategoryAdapter()
        binding.btnCreateCategory.setOnClickListener {
            vm.onCreateCategory()
        }
    }

    private fun setUpCategoryAdapter() {
        categoryAdapter.categoryClickListener = this
        binding.rvCategories.apply {
            adapter = categoryAdapter
        }
        ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val category = categoryAdapter.currentList[viewHolder.bindingAdapterPosition]
                vm.onCategorySwiped(category)
            }
        }).attachToRecyclerView(binding.rvCategories)
    }

    private fun subscribeToObservers() {
        vm.categories.observe(viewLifecycleOwner) {
            categoryAdapter.submitList(it)
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.categoryEvent.collect { event ->
                when (event) {
                    CategoryVM.CategoryEvent.ShowCreateCategoryDialog -> {
                        createEditCategoryDialog = CreateEditCategoryDialog()
                        createEditCategoryDialog?.createCategoryEventListener =
                            this@CategoryFragment
                        createEditCategoryDialog?.show(childFragmentManager, null)
                    }
                    is CategoryVM.CategoryEvent.ShowEditCategoryDialog -> {
                        createEditCategoryDialog = CreateEditCategoryDialog(event.category)
                        createEditCategoryDialog?.createCategoryEventListener =
                            this@CategoryFragment
                        createEditCategoryDialog?.show(childFragmentManager, null)
                    }
                    is CategoryVM.CategoryEvent.ShowInvalidInputMessage -> {
                        showSnackBar(event.msg)
                    }
                    CategoryVM.CategoryEvent.ShowCategorySavedConfirmationMessage -> {
                        createEditCategoryDialog?.dismiss()
                        rvCategories.smoothScrollToPosition(categoryAdapter.currentList.size)
                    }
                    CategoryVM.CategoryEvent.ShowCategoryUpdatedMessage -> {
                        createEditCategoryDialog?.dismiss()
                        showSnackBar(getString(R.string.category_updated_successfully))
                    }
                    is CategoryVM.CategoryEvent.ShowUndoDeleteCategoryMessage -> {
                        Snackbar.make(
                            requireView(),
                            getString(R.string.category_deleted),
                            Snackbar.LENGTH_LONG
                        )
                            .setAction("انصراف") {
                                vm.undoDeleteClick(event.category)
                            }.show()
                    }
                }
            }
        }
    }

    override fun onCategoryClick(category: Category) {
        vm.onEditCategory(category)
    }

    override fun createCategory(categoryName: String) {
        vm.createCategory(categoryName)
    }

    override fun updateCategory(category: Category) {
        vm.updateCategory(category)
    }
}