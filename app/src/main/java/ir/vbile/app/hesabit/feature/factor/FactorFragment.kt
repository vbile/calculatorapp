package ir.vbile.app.hesabit.feature.factor

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.factor.Factor
import ir.vbile.app.hesabit.databinding.FragmentFactorBinding
import ir.vbile.app.hesabit.extentions.afterTextChanged
import ir.vbile.app.hesabit.extentions.logViewFactor
import ir.vbile.app.hesabit.feature.factor.adapters.FactorAdapter
import ir.vbile.app.hesabit.feature.factor.dialogs.CreateFactorDialog
import kotlinx.android.synthetic.main.fragment_factor.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class FactorFragment : BaseFragment<FactorVM>(R.layout.fragment_factor, FactorVM::class),
    FactorAdapter.OnFactorClickListener, CreateFactorDialog.CreateDialogListener {
    @Inject
    lateinit var factorAdapter: FactorAdapter
    private lateinit var binding: FragmentFactorBinding
    lateinit var createFactorDialog: CreateFactorDialog
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentFactorBinding.bind(view)
        createFactorDialog = CreateFactorDialog()
        createFactorDialog.createDialogListener = this
        factorAdapter.factorClickListener = this
        binding.rvFactors.apply {
            adapter = factorAdapter
        }
        btnCreateFactor.setOnClickListener {
            createFactorDialog.show(this.childFragmentManager, null)
        }
        binding.etKeyword.afterTextChanged {
            vm.filterFactors(it)
        }
        subscribeToObservers()
    }

    private fun subscribeToObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.uiState.collectLatest { uiState ->
                when (uiState) {
                    is BaseVM.UiState.Loading -> {
                        setProgressIndicator(uiState.shouldShow)
                    }
                    is BaseVM.UiState.EmptyState -> {
                        /*showEmptyState(
                            mustShow = uiState.shouldShow,
                            description = getString(R.string.factor_empty_state_desc),
                            btnText = getString(R.string.factor_empty_state_action)
                        ) {
                            // request again
                        }*/
                    }
                }
            }
        }
        vm.factors.observe(viewLifecycleOwner) {
            factorAdapter.submitList(it)
        }
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.factorEvent.collect { event ->
                when (event) {
                    is FactorVM.FactorEvent.NavigateToCreateFactorScreen -> {
                        createFactorDialog.dismiss()
                        val action =
                            FactorFragmentDirections.actionFactorFragmentToCreateFactorFragment(
                                event.category,
                                event.customerName,
                                event.factorId,
                                event.update
                            )
                        findNavController().navigate(action)
                    }
                    is FactorVM.FactorEvent.NavigateToAddEditFactorScreen -> {
                        val action =
                            FactorFragmentDirections.actionFactorFragmentToCreateFactorFragment(
                                event.category,
                                event.customerName,
                                event.factorId,
                                event.update
                            )
                        findNavController().navigate(action)
                    }
                }
            }
        }
    }

    override fun onFactorClick(factor: Factor) {
        vm.onFactorClick(factor)
        firebaseAnalytics.logViewFactor(factor.id.toString())
    }

    override fun createDialog(customerName: String) {
        val category = vm.category.value ?: throw IllegalArgumentException("")
        vm.onAddNewFactorClick(category, customerName)
    }
}