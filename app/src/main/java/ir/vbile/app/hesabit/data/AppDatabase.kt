package ir.vbile.app.hesabit.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.category.CategoryDao
import ir.vbile.app.hesabit.data.factor.Factor
import ir.vbile.app.hesabit.data.factor.FactorDao
import ir.vbile.app.hesabit.data.factor.item.FactorItem
import ir.vbile.app.hesabit.data.factor.item.FactorItemDao
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.data.product.ProductDao
import ir.vbile.app.hesabit.di.ApplicationScope
import ir.vbile.app.hesabit.other.Constants.DEFAULT_PRODUCT_PRICE
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider
import kotlin.math.floor

@Database(
    entities = [
        Category::class,
        Factor::class,
        Product::class,
        FactorItem::class
    ], version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun factorDao(): FactorDao
    abstract fun productDao(): ProductDao
    abstract fun factorItemDao(): FactorItemDao
    class CallBack @Inject constructor(
        private val database: Provider<AppDatabase>,
        @ApplicationScope private val applicationScope: CoroutineScope
    ) : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            val categoryDao = database.get().categoryDao()
            val factorDao = database.get().factorDao()
            val productDao = database.get().productDao()
            val factorItemDao = database.get().factorItemDao()
            applicationScope.launch {
//                populateCategories(categoryDao)
//                populateProducts(productDao)
//                populateFactor(factorDao, categoryDao)
            }
        }

        private suspend fun populateProducts(
            productDao: ProductDao
        ) {
            productDao.insert(Product("تیغه", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("موتور", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("لوله", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("دستمزد", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("قوطی", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("کاور", DEFAULT_PRODUCT_PRICE, 1))
            productDao.insert(Product("جا قفلی",DEFAULT_PRODUCT_PRICE, 1))
        }

        private suspend fun populateFactor(
            factorDao: FactorDao,
            categoryDao: CategoryDao
        ) {
            for (i in 1..10) {
                val random = getRandom(1, 6)
                val category = categoryDao.getCategories().first()[random]
                factorDao.insert(
                    Factor(
                        "مشتری شماره $i",
                        category.id
                    )
                )
            }
        }

        private suspend fun populateCategories(dao: CategoryDao) {
            dao.insert(Category("کرکره برفی"))
            dao.insert(Category("دوربین مدار بسته"))
            dao.insert(Category("جک پارکینگ"))
            dao.insert(Category("دزدگیر"))
            dao.insert(Category("درب اتوماتیک شیشه ای"))
            dao.insert(Category("درب شیشه ای دستی"))
            dao.insert(Category("سایبان برقی"))
        }
    }
}


private fun getRandom(min: Int, max: Int): Int {
    return floor(Math.random() * (max - min + 1)).toInt() + min
}
