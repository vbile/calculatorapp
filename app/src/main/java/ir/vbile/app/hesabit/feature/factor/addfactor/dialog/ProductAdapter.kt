package ir.vbile.app.hesabit.feature.factor.addfactor.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.product.ProductUIModel
import ir.vbile.app.hesabit.databinding.ItemSelectProductBinding

class ProductAdapter : ListAdapter<ProductUIModel, BaseViewHolder>(DIFF_CALLBACK) {

    private var onProductClickListener: ((item: ProductUIModel) -> Unit)? = null

    fun setOnProductClickListener(listener: (item: ProductUIModel) -> Unit) {
        onProductClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ProductViewHolder(
            ItemSelectProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    private inner class ProductViewHolder(val binding: ItemSelectProductBinding) :
        BaseViewHolder(binding.root) {
        override fun bind(position: Int) = binding.root.run {
            val product = getItem(position) ?: return@run
            binding.root.setOnClickListener {
                if (layoutPosition == RecyclerView.NO_POSITION) return@setOnClickListener
                binding.chbSelected.isChecked = !product.selected
                onProductClickListener?.invoke(product)
            }
            binding.chbSelected.setOnClickListener {
                binding.root.performClick()
//                product.selected = !product.selected
//                binding.chbSelected.isChecked = isChecked
//                onProductClickListener?.invoke(product)
            }
            binding.chbSelected.isChecked = product.selected
            binding.tvName.text = product.name
            binding.tvNumber.text = (position + 1).toString()
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ProductUIModel>() {
            override fun areItemsTheSame(
                oldItem: ProductUIModel,
                newItem: ProductUIModel
            ): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: ProductUIModel,
                newItem: ProductUIModel
            ): Boolean {
                return oldItem.selected == newItem.selected
            }
        }
    }
}