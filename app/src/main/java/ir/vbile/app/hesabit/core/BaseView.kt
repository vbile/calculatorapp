package ir.vbile.app.hesabit.core

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import ir.vbile.app.hesabit.R

interface BaseView {
    val rootView: CoordinatorLayout?
    val viewContext: Context?
    fun setProgressIndicator(mustShow: Boolean) {
        rootView?.let {
            viewContext?.let { context ->
                var loadingView = it.findViewById<View>(R.id.loadingView)
                if (loadingView == null && mustShow) {
                    loadingView =
                        LayoutInflater.from(context).inflate(R.layout.view_loading, it, false)
                    it.addView(loadingView)
                }
                loadingView?.visibility = if (mustShow) View.VISIBLE else View.GONE
            }
        }
    }

    fun showEmptyState(
        mustShow: Boolean,
        description: String? = null,
        btnText: String? = null,
        action: () -> Unit = {}
    ) {
        rootView?.let {
            viewContext?.let { context ->
                var emptyStateView = it.findViewById<View>(R.id.emptyStateView)
                val btnTryAgain: MaterialButton
                val tvDescription: TextView
                if (emptyStateView == null && mustShow) {
                    emptyStateView =
                        LayoutInflater.from(context).inflate(R.layout.view_empty_state, it, false)
                    it.addView(emptyStateView)
                    btnTryAgain = it.findViewById(R.id.btnTryAgain)
                    btnText?.let {
                        btnTryAgain.text = it
                    }
                    tvDescription = it.findViewById(R.id.tvDescription)
                    description?.let {
                        tvDescription.text = it
                    }
                    btnTryAgain?.setOnClickListener {
                        action()
                    }
                }
                emptyStateView?.visibility = if (mustShow) View.VISIBLE else View.GONE
            }
        }
    }

    fun showSnackBar(message: String, duration: Int = Snackbar.LENGTH_SHORT) {
        rootView?.let {
            viewContext?.let { context ->
                Snackbar.make(it, message, duration).show()
            }
        }
    }

    fun longToast(string: String) {
        Toast.makeText(viewContext, string, Toast.LENGTH_LONG).show()
    }

    fun toast(string: String) {
        Toast.makeText(viewContext, string, Toast.LENGTH_SHORT).show()
    }
}