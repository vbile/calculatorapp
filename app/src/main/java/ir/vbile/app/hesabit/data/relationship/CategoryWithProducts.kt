package ir.vbile.app.hesabit.data.relationship

import androidx.room.Embedded
import androidx.room.Relation
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.product.Product

data class CategoryWithProducts(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "id",
        entityColumn = "categoryId"
    )
    val products: List<Product>
)
