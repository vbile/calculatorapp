package ir.vbile.app.hesabit.extentions

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import java.text.NumberFormat
import java.util.*


typealias MethodBlock<T> = T.() -> Unit

fun <E> Iterable<E>.replace(old: E, new: E) = map { if (it == old) new else it }


fun <T> List<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
    return map {
        if (block(it)) newValue else it
    }
}

// for mutable list
operator fun <T> MutableLiveData<MutableList<T>>.plusAssign(item: T) {
    val value = this.value ?: mutableListOf()
    value.add(item)
    this.value = value
}

fun formatPrice(value: Float): String {
    try {
        return NumberFormat.getInstance(Locale.US).format(value)
    } catch (ex: NumberFormatException) {
        throw Exception("")
    } catch (e: Exception) {
        throw Exception("")
    }
}


fun Context.openInstagramApp() {
    val uri = Uri.parse("https://www.instagram.com/vahid.dev");
    val insta = Intent(Intent.ACTION_VIEW, uri);
    insta.setPackage("com.instagram.android");
    if (isIntentAvailable(this, insta)) {
        startActivity(insta);
    } else {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/vahid.dev")));
    }
}

fun isIntentAvailable(ctx: Context, intent: Intent): Boolean {
    val packageManager = ctx.packageManager
    val list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
    return list.size > 0
}

fun Context.openWhatsApp(){
    val url = "https://api.whatsapp.com/send?phone=989102999274"
    val i = Intent(Intent.ACTION_VIEW)
    i.data = Uri.parse(url)
    startActivity(i)
}