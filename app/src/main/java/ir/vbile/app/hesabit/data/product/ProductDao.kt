package ir.vbile.app.hesabit.data.product

import androidx.room.*
import ir.vbile.app.hesabit.data.relationship.CategoryWithProducts
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductDao {

    @Query("SELECT * FROM tbl_product")
    fun getProducts(): Flow<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(product: Product)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(product: Product) : Int

    @Delete
    suspend fun delete(product: Product)


    @Transaction
    @Query("SELECT * FROM tbl_category WHERE id = :categoryId")
    fun getProductsWithCategoryId(categoryId: Long): Flow<CategoryWithProducts>

}