package ir.vbile.app.hesabit.data.converters

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types.newParameterizedType
import ir.vbile.app.hesabit.data.factor.item.FactorItem

val moshi = Moshi.Builder().build()
val dataListType = newParameterizedType(List::class.java, FactorItem::class.java)
val adapter: JsonAdapter<List<FactorItem>> = moshi.adapter(dataListType)

class FactorItemConverter {

    @TypeConverter
    fun stringToFactorITems(string: String): List<FactorItem> {
        return adapter.fromJson(string).orEmpty()
    }


    @TypeConverter
    fun factorItemsToString(members: List<FactorItem>): String {
        return adapter.toJson(members)
    }
}