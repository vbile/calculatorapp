package ir.vbile.app.hesabit.core

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind() {
        if (layoutPosition != RecyclerView.NO_POSITION) bind(layoutPosition)
    }
    protected abstract fun bind(position: Int)
}