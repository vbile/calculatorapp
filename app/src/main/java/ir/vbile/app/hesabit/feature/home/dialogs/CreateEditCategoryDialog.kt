package ir.vbile.app.hesabit.feature.home.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.databinding.DialogCreateCategoryBinding
import ir.vbile.app.hesabit.extentions.afterTextChanged

class CreateEditCategoryDialog constructor(
    private val category : Category? = null
) : BottomSheetDialogFragment() {
    lateinit var binding: DialogCreateCategoryBinding
    var createCategoryEventListener: CreateCategoryEventListener? = null
    lateinit var name: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_create_category, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogCreateCategoryBinding.bind(view)
        binding.etCategoryName.afterTextChanged {
            name = it
        }
        binding.btnCreateCategory.setOnClickListener {
            category?.let {
                it.name = name
                createCategoryEventListener?.updateCategory(it)
                return@setOnClickListener
            }
            createCategoryEventListener?.createCategory(name)
        }
        category?.let {
            binding.etCategoryName.setText(it.name)
            binding.btnCreateCategory.text = getString(R.string.update_category)
        }
    }

    interface CreateCategoryEventListener {
        fun createCategory(categoryName: String)
        fun updateCategory(category: Category)
    }

    override fun getTheme(): Int = R.style.AppBottomSheetDialogTheme

}