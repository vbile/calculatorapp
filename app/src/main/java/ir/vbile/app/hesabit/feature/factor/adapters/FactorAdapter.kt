package ir.vbile.app.hesabit.feature.factor.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.factor.Factor
import ir.vbile.app.hesabit.databinding.ItemFactorBinding
import ir.vbile.app.hesabit.extentions.english2Persian
import ir.vbile.app.hesabit.extentions.implementSpringAnimationTrait
import kotlinx.android.synthetic.main.item_category.view.*

class FactorAdapter : ListAdapter<Factor, BaseViewHolder>(CallBack()) {

    var factorClickListener: OnFactorClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemFactorBinding.bind(
            LayoutInflater.from(parent.context).inflate(R.layout.item_factor, parent, false)
        )
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    private inner class CategoryViewHolder(val binding: ItemFactorBinding) :
        BaseViewHolder(binding.root) {
        override fun bind(position: Int) {
            binding.apply {
                val factor = getItem(position) ?: return
                root.implementSpringAnimationTrait()
                root.setOnClickListener {
                    factorClickListener?.onFactorClick(factor)
                }
                tvName.text = factor.name
                tvNumber.text = (position + 1).toString().english2Persian()
            }
        }
    }

    companion object {
        class CallBack : DiffUtil.ItemCallback<Factor>() {
            override fun areItemsTheSame(oldItem: Factor, newItem: Factor): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Factor, newItem: Factor): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }

    interface OnFactorClickListener {
        fun onFactorClick(factor: Factor)
    }
}