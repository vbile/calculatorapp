package ir.vbile.app.hesabit.core

import android.content.Context
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import kotlin.reflect.KClass

abstract class BaseFragment<VM : BaseVM>(
    @LayoutRes val layoutRes: Int,
    private val vmClass: KClass<VM>
) : Fragment(layoutRes), BaseView {
    override val rootView: CoordinatorLayout?
        get() = view as CoordinatorLayout?

    override val viewContext: Context?
        get() = context

    val firebaseAnalytics by lazy {
        FirebaseAnalytics.getInstance(requireActivity())
    }

    @MainThread
    private fun viewModels(
        ownerProducer: () -> ViewModelStoreOwner = { this },
        factoryProducer: (() -> ViewModelProvider.Factory)? = null
    ) = createViewModelLazy(vmClass, { ownerProducer().viewModelStore }, factoryProducer)

    open fun getViewModelStoreOwner(): ViewModelStoreOwner = this

    protected val vm: VM by viewModels({ getViewModelStoreOwner() })

    private fun setCurrentScreen(screenName: String) = firebaseAnalytics.run {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, this@BaseFragment.javaClass.simpleName)
        }
    }

    override fun onResume() {
        super.onResume()
        setCurrentScreen(this.javaClass.simpleName)
    }
}