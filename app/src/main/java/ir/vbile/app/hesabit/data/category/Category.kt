package ir.vbile.app.hesabit.data.category

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.text.DateFormat

@Entity(tableName = "tbl_category")
@Parcelize
@JsonClass(generateAdapter = true)
data class Category(
    var name: String,
    val created: Long = System.currentTimeMillis(),
    @PrimaryKey(autoGenerate = true) val id: Long = 0
) : Parcelable {
    val createdDateFormatted: String
        get() = DateFormat.getDateInstance().format(created)
}