package ir.vbile.app.hesabit.data.category

import androidx.room.*
import ir.vbile.app.hesabit.data.factor.Factor
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {

    @Query("SELECT * FROM tbl_category")
    fun getCategories(): Flow<List<Category>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: Category)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(category: Category)

    @Delete
    suspend fun delete(category: Category)

    @Transaction
    @Query("SELECT * FROM tbl_factor WHERE categoryId = :categoryId")
    fun getCategoryWithFactors(categoryId: Long): Flow<List<Factor>>
}