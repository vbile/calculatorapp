package ir.vbile.app.hesabit.extentions

import java.util.*

fun getGreetingMessage(): String {
    val c = Calendar.getInstance()
    return when (c.get(Calendar.HOUR_OF_DAY)) {
        in 0..5 -> "وقت بخیر"
        in 6..15 -> "ظهر بخیر"
        in 16..19 -> "عصر بخیر"
        in 19..23 -> "شب بخیر"
        else -> "وقت بخیر"
    }
}