package ir.vbile.app.hesabit.data.factor.item

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import com.squareup.moshi.JsonClass
import ir.vbile.app.hesabit.data.product.Product
import kotlinx.android.parcel.Parcelize
import java.text.DateFormat

@JsonClass(generateAdapter = true)
@Parcelize
@Entity(
    tableName = "tbl_factor_items",
    primaryKeys = ["product_name", "factorId"],
    indices = [Index("product_name")]
)
data class FactorItem(
    @Embedded(prefix = "product_") var product: Product,
    var count: Int = 0,
    val factorId: Long,
    var totalPrice: Float = count * product.price,
    val created: Long = System.currentTimeMillis(),
    val updated: Long = System.currentTimeMillis(),
) : Parcelable {
    val createdDateFormatted: String
        get() = DateFormat.getDateInstance().format(created)
    val updatedDateFormatted: String
        get() = DateFormat.getDateInstance().format(updated)
}