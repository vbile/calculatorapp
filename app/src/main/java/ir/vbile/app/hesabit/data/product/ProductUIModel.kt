package ir.vbile.app.hesabit.data.product

import ir.vbile.app.hesabit.data.factor.item.FactorItem
import java.text.DateFormat

data class ProductUIModel(
    var name: String,
    var price: Float,
    var categoryId: Long,
    val created: Long = System.currentTimeMillis(),
    var selected: Boolean
){
    val createdDateFormatted: String
        get() = DateFormat.getDateInstance().format(created)
    fun toModel() = Product(name, price, categoryId, created)
}