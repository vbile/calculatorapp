package ir.vbile.app.hesabit.feature.setting

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import javax.inject.Inject

@HiltViewModel
class SettingVM @Inject constructor() : BaseVM() {
}