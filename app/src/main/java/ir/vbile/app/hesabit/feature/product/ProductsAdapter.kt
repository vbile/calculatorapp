package ir.vbile.app.hesabit.feature.product

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseViewHolder
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.databinding.ItemProductBinding
import ir.vbile.app.hesabit.extentions.implementSpringAnimationTrait


class ProductsAdapter : ListAdapter<Product, BaseViewHolder>(Callback()) {

    companion object {
        class Callback : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }

    private var listener: ProductsEventListener? = null
    fun setOnProductEventListener(listener: ProductsEventListener? = null) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding = ItemProductBinding.bind(
            LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        )
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.bind()

    inner class ProductViewHolder(val binding: ItemProductBinding) : BaseViewHolder(binding.root) {
        override fun bind(position: Int) {
            val product = getItem(position) ?: return
            binding.root.implementSpringAnimationTrait()
            binding.root.setOnClickListener {
                listener?.onProductClicked(product)
            }
            binding.tvName.text = product.name
            binding.tvNumber.text = product.id.toString()
            binding.tvPrice.text = binding.root.context.getString(
                R.string.item_calculator_price,
                product.price.toInt()
            )
        }
    }

    interface ProductsEventListener {
        fun onProductClicked(product: Product)
    }
}