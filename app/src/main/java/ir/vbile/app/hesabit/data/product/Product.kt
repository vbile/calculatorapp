package ir.vbile.app.hesabit.data.product

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.text.DateFormat

@Parcelize
@Entity(tableName = "tbl_product", indices = [Index("name", unique = true)])
@JsonClass(generateAdapter = true)
data class Product(
    var name: String,
    var price: Float,
    var categoryId: Long,
    val created: Long = System.currentTimeMillis(),
    @PrimaryKey(autoGenerate = true) val id: Long = 0
) : Parcelable {
    val createdDateFormatted: String
        get() = DateFormat.getDateInstance().format(created)

    fun toModel() = ProductUIModel(
        name, price, created, created, false
    )
}