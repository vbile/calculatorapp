package ir.vbile.app.hesabit.data.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.squareup.moshi.JsonClass
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.factor.Factor

@JsonClass(generateAdapter = true)
data class CategoryWithFactors(
    @Embedded
    val category: Category,
    @Relation(
        entityColumn = "categoryId",
        parentColumn = "id"
    )
    val factors: List<Factor>
)