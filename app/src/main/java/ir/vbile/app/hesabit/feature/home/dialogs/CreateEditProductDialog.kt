package ir.vbile.app.hesabit.feature.home.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.product.Product
import ir.vbile.app.hesabit.databinding.DialogCreateProductBinding
import ir.vbile.app.hesabit.extentions.Constants
import ir.vbile.app.hesabit.extentions.addCurrencyFormatter
import ir.vbile.app.hesabit.extentions.afterTextChanged
import ir.vbile.app.hesabit.extentions.formatPrice
import kotlin.properties.Delegates

class CreateEditProductDialog constructor(
    val categories: List<Category>,
    val product: Product? = null
) : BottomSheetDialogFragment() {
    lateinit var binding: DialogCreateProductBinding
    var createProductEventListener: CreateProductEventListener? = null
    lateinit var name: String
    var price by Delegates.notNull<Float>()
    lateinit var categoryAdapter: ArrayAdapter<String>
    lateinit var category: Category
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_create_product, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogCreateProductBinding.bind(view)
        categoryAdapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line)
        categoryAdapter.addAll(categories.map { it.name })
        binding.etProductName.afterTextChanged {
            name = it
        }
        binding.etBasePrice.afterTextChanged {
            price = if (it.contains(Constants.CURRENCY_SEPARATOR)) {
                it.replace(Constants.CURRENCY_SEPARATOR, "").toFloatOrNull() ?: 0f
            } else it.toFloatOrNull() ?: 0f
        }
        binding.etBasePrice.addCurrencyFormatter()
        binding.spinnerCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    category = categories[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            }
        binding.spinnerCategory.adapter = categoryAdapter
        binding.btnCreateCategory.setOnClickListener {
            createProductEventListener?.saveProduct(category, name, price)
            product?.let {
                it.name = name
                it.price = price
                it.categoryId = category.id
                createProductEventListener?.updateProduct(it)
            }
        }
        product?.let {
            binding.etProductName.setText(it.name)
            binding.etBasePrice.setText(formatPrice(it.price))
            val category: Category? =
                categories.find { category -> category.id == product.categoryId }
            val position = categories.indexOf(category)
            binding.spinnerCategory.setSelection(position)
            binding.tvTitle.text = getString(R.string.edit_product)
            name = product.name
            price = product.price
        }
    }

    interface CreateProductEventListener {
        fun saveProduct(category: Category, productName: String, price: Float)
        fun updateProduct(product: Product)
    }

    override fun getTheme(): Int = R.style.AppBottomSheetDialogTheme

}