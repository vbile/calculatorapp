package ir.vbile.app.hesabit.extentions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import ir.vbile.app.hesabit.extentions.Constants.CURRENCY_SEPARATOR
import ir.vbile.app.hesabit.extentions.Constants.ZERO_NUMBER
import timber.log.Timber
import java.text.NumberFormat
import java.util.*

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun afterTextChanged(editable: Editable?) =
            afterTextChanged.invoke(editable.toString())
    })
}

fun EditText.addCurrencyFormatter() {
    this.addTextChangedListener(object : TextWatcher {
        private var current = ""
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            this@addCurrencyFormatter.removeTextChangedListener(this)
            var newValue = s.toString()
            if (newValue.contains(CURRENCY_SEPARATOR)) {
                newValue = newValue.replace(CURRENCY_SEPARATOR, "")
            }
            if (newValue != current) {
                if (newValue.isEmpty()) {
                    newValue = ZERO_NUMBER
                } else if (newValue.length > 1) {
                    if (newValue.startsWith(ZERO_NUMBER)) {
                        newValue = newValue.substring(1, newValue.length)
                    }
                }
                try {
                    newValue = NumberFormat.getInstance(Locale.US).format(newValue.toLong())
                } catch (ex: NumberFormatException) {
                    Timber.e(ex)
                } catch (e: Exception) {
                    Timber.e(e)
                }
                current = newValue
                this@addCurrencyFormatter.setText(current)
                this@addCurrencyFormatter.setSelection(current.length)
                this@addCurrencyFormatter.addTextChangedListener(this)
            }else{
                Timber.i("")
            }
        }
    })
}

fun EditText.preventEnteringZeroNumberAtFist() {
    var current = ""
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(input: CharSequence?, start: Int, before: Int, count: Int) {
            this@preventEnteringZeroNumberAtFist.removeTextChangedListener(this)
            val newValue = input.toString()
            current = newValue
            if (newValue.isEmpty()) {
                current = ZERO_NUMBER
            } else if (newValue.length == 1 && newValue.startsWith("0")) {
                current = ZERO_NUMBER
            } else if (newValue.length > 1) {
                if (newValue.startsWith(ZERO_NUMBER)) {
                    current = newValue.substring(1, newValue.length)
                } else {
                    current = newValue
                }
            }
            this@preventEnteringZeroNumberAtFist.setText(current)
            this@preventEnteringZeroNumberAtFist.setSelection(current.length)
            this@preventEnteringZeroNumberAtFist.addTextChangedListener(this)
        }

        override fun afterTextChanged(s: Editable?) {}
    })
}


object Constants {
    const val CURRENCY_SEPARATOR = ","
    const val ZERO_NUMBER = "0"
}