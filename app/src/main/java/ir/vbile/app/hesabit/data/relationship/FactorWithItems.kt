package ir.vbile.app.hesabit.data.relationship

import androidx.room.Embedded
import androidx.room.Relation
import ir.vbile.app.hesabit.data.factor.Factor
import ir.vbile.app.hesabit.data.factor.item.FactorItem

data class FactorWithItems(
    @Embedded val factor: Factor,
    @Relation(
        parentColumn = "id",
        entityColumn = "factorId"
    )
    val items: List<FactorItem>
)