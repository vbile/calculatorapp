package ir.vbile.app.hesabit.data.factor.item

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface FactorItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(factorItem: FactorItem): Long

    @Delete
    suspend fun delete(factorItem: FactorItem)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(factorItem: FactorItem)

    @Query("SELECT * FROM tbl_factor_items WHERE factorId = :factorId")
    fun factorWithItems(factorId: Long): Flow<List<FactorItem>>

    @Query("DELETE FROM tbl_factor_items WHERE product_name = :productName  AND factorId =:factorId")
    fun deleteByProductName(productName: String, factorId: Long)
}