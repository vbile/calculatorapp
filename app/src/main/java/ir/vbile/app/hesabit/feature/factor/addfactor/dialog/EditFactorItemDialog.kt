package ir.vbile.app.hesabit.feature.factor.addfactor.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.data.factor.item.FactorItem
import ir.vbile.app.hesabit.databinding.ModalEditFactorItemBinding
import ir.vbile.app.hesabit.extentions.*

class EditFactorItemDialog constructor(
    private val factorItem: FactorItem
) : BottomSheetDialogFragment() {
    lateinit var binding: ModalEditFactorItemBinding
    private var count = factorItem.count.toString()
    private var price = factorItem.product.price.toString()
    private val totalPrice = MutableLiveData(count.toFloat() * price.toFloat())
    private var onUpdate: (FactorItem) -> Unit = {}
    fun addOnUpdateListener(listener: (FactorItem) -> Unit) {
        onUpdate = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ModalEditFactorItemBinding.inflate(inflater).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ModalEditFactorItemBinding.bind(view)
        binding.etPrice.addCurrencyFormatter()
        binding.etCount.preventEnteringZeroNumberAtFist()
        binding.etPrice.setText(formatPrice(factorItem.product.price))
        binding.etCount.setText(factorItem.count.toString())
        binding.etCount.afterTextChanged {
            count = it
            calculateTotalPrice()
        }
        binding.etPrice.afterTextChanged {
            price = it
            calculateTotalPrice()
        }
        totalPrice.observe(viewLifecycleOwner) {
            binding.tvTotalPrice.text = getString(R.string.total_price_value, formatPrice(it))
        }

        binding.etlPrice.setEndIconOnClickListener {
            if (binding.etPrice.text.toString() == "0") return@setEndIconOnClickListener
            price = "0"
            binding.etPrice.setText(price)
            calculateTotalPrice()
        }
        binding.etlCount.setEndIconOnClickListener {
            count = "0"
            binding.etCount.setText(count)
            calculateTotalPrice()
        }
        binding.btnUpdate.setOnClickListener {
            factorItem.count = count.toIntOrNull() ?: 0
            factorItem.totalPrice = (count.toFloatOrNull() ?: 0f) * (price.toFloatOrNull() ?: 0f)
            factorItem.product.price = price.toFloatOrNull() ?: 0f
            onUpdate(factorItem)
        }
    }

    private fun calculateTotalPrice() {
        if (price.contains(Constants.CURRENCY_SEPARATOR)) {
            price = price.replace(Constants.CURRENCY_SEPARATOR, "")
        }
        if (count.isEmpty() || price.isEmpty()) {
            return
        }
        val totalPrice = count.toFloat() * price.toFloat()
        this.totalPrice.value = totalPrice
    }

    companion object {
        const val TAG = "EditFactorItemDialog"
    }

    override fun getTheme(): Int = R.style.AppBottomSheetDialogTheme
}