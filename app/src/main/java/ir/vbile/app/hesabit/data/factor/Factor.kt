package ir.vbile.app.hesabit.data.factor

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize
import java.text.DateFormat

@Entity(tableName = "tbl_factor")
@Parcelize
@JsonClass(generateAdapter = true)
data class Factor(
    val name: String,
    val categoryId: Long,
    val created: Long = System.currentTimeMillis(),
    @PrimaryKey(autoGenerate = true) val id: Long = 0
) : Parcelable {
    val createdDateFormatted: String
        get() = DateFormat.getDateInstance().format(created)
}