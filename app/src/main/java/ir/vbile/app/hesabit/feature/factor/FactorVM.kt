package ir.vbile.app.hesabit.feature.factor

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.vbile.app.hesabit.core.BaseVM
import ir.vbile.app.hesabit.data.category.Category
import ir.vbile.app.hesabit.data.category.CategoryDao
import ir.vbile.app.hesabit.data.factor.Factor
import ir.vbile.app.hesabit.data.factor.FactorDao
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FactorVM @Inject constructor(
    val factorDao: FactorDao,
    categoryDao: CategoryDao,
    val state: SavedStateHandle
) : BaseVM() {
    val category = state.getLiveData<Category>("category")
    val factorEventChannel = Channel<FactorEvent>()
    private val factorFlow = categoryDao.getCategoryWithFactors(category.value?.id ?: throw Exception("Category id"))
            .apply {
                viewModelScope.launch {
                    collectLatest {
                        if (it.isNotEmpty()) {
                            _uiState.emit(UiState.Loading(false))
                            _uiState.emit(UiState.EmptyState(false))
                        } else {
                            _uiState.emit(UiState.EmptyState(true))
                            _uiState.emit(UiState.Loading(false))
                        }
                    }
                }
            }
    val factorEvent = factorEventChannel.receiveAsFlow()
    val factors = factorFlow.asLiveData()


    fun onAddNewFactorClick(category: Category, customerName: String) = viewModelScope.launch {
        val factorId = factorDao.insert(Factor(customerName, category.id))
        factorEventChannel.send(
            FactorEvent.NavigateToCreateFactorScreen(
                category = category,
                factorId = factorId,
                customerName = customerName,
                update = true
            )
        )
    }

    fun onFactorClick(factor: Factor) = viewModelScope.launch {
        val category = category.value ?: throw IllegalArgumentException("")
        factorEventChannel.send(
            FactorEvent.NavigateToAddEditFactorScreen(
                category = category,
                factorId = factor.id,
                customerName = factor.name,
                update = false
            )
        )
    }
    fun filterFactors(query : String) = viewModelScope.launch{
        factorDao.filterFactors(query)
    }

    sealed class FactorEvent {
        data class NavigateToCreateFactorScreen(
            val category: Category,
            val factorId: Long,
            val customerName: String,
            val update: Boolean
        ) : FactorEvent()

        data class NavigateToAddEditFactorScreen(
            val category: Category,
            val factorId: Long,
            val customerName: String,
            val update: Boolean
        ) : FactorEvent()
    }
}