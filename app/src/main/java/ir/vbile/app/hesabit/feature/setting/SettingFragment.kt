package ir.vbile.app.hesabit.feature.setting

import android.os.Bundle
import android.view.View
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.databinding.FragmentSettingBinding
import ir.vbile.app.hesabit.extentions.openWhatsApp

@AndroidEntryPoint
class SettingFragment : BaseFragment<SettingVM>(R.layout.fragment_setting, SettingVM::class) {
    private lateinit var binding: FragmentSettingBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSettingBinding.bind(view)
        binding.btnContact.setOnClickListener {
            requireContext().openWhatsApp()
        }
    }
}