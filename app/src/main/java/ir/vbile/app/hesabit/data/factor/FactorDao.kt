package ir.vbile.app.hesabit.data.factor

import androidx.room.*
import ir.vbile.app.hesabit.data.relationship.FactorWithItems
import kotlinx.coroutines.flow.Flow

@Dao
interface FactorDao {

    @Query("SELECT * FROM tbl_factor")
    fun getFactors(): Flow<List<Factor>>


    @Query("SELECT * FROM tbl_factor WHERE id = :factorId")
    fun getFactor(factorId: Long): Flow<Factor>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(factor: Factor): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(factor: Factor)

    @Delete
    suspend fun delete(factor: Factor)

    @Query("SELECT * FROM tbl_factor WHERE categoryId = :categoryID")
    fun getFactorByCategoryID(categoryID: Int): Flow<Factor>

    @Transaction
    @Query("SELECT * FROM tbl_factor WHERE id = :factorId")
    fun getFactorWithItems(factorId: Long): Flow<FactorWithItems>

    @Transaction
    @Query("SELECT * FROM tbl_factor WHERE name LIKE '%' || :searchQuery || '%'")
    fun filterFactors(searchQuery: String): Flow<List<Factor>>
}