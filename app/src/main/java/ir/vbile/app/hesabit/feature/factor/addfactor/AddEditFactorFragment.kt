package ir.vbile.app.hesabit.feature.factor.addfactor

import android.Manifest
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.muddzdev.quickshot.QuickShot
import dagger.hilt.android.AndroidEntryPoint
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.core.BaseFragment
import ir.vbile.app.hesabit.data.factor.item.FactorItem
import ir.vbile.app.hesabit.databinding.FragmentCreateFactorBinding
import ir.vbile.app.hesabit.extentions.formatPrice
import ir.vbile.app.hesabit.feature.factor.addfactor.dialog.EditFactorItemDialog
import ir.vbile.app.hesabit.feature.factor.addfactor.dialog.ProductAdapter
import ir.vbile.app.hesabit.other.Constants.REQUEST_CODE_WRITE_EXTERNAL_STORAGE
import ir.vbile.app.hesabit.other.PermissionUtility
import kotlinx.android.synthetic.main.fragment_create_factor.*
import kotlinx.coroutines.flow.collect
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import java.util.*

@AndroidEntryPoint
class AddEditFactorFragment : BaseFragment<AddEditFactorVM>(
    R.layout.fragment_create_factor,
    AddEditFactorVM::class
),
    FactorItemAdapter.OnFactorItemClickListener, QuickShot.QuickShotListener,
    EasyPermissions.PermissionCallbacks {
    var factorItemAdapter: FactorItemAdapter = FactorItemAdapter(this)
    lateinit var binding: FragmentCreateFactorBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    var productAdapter = ProductAdapter()
    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                Timber.i("permission granted")
            } else {
                if (EasyPermissions.somePermissionPermanentlyDenied(this, listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
                    AppSettingsDialog.Builder(this).build().show()
                } else {
                    requestPermission()
                }
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCreateFactorBinding.bind(view)
        setUpViews()
        setUpFactorItems()
        setUpProductAdapter()
        setupStandardBottomSheet()
        subscribeToObservers()
    }

    private fun setUpViews() {
        btnAddProduct.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        binding.btnAddProducts.setOnClickListener {
//            vm.addProductsToFactor()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        binding.btnSaveFactor.setOnClickListener {
        }
        binding.cbSelectAll.setOnClickListener {
            if (binding.cbSelectAll.isChecked) {
                vm.selectProducts()
            } else
                vm.deselectProducts()
        }
        binding.btnCloseDialog.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
        binding.tvTotalPrice.setOnClickListener {
            printFactor()
        }
        toolbar.setTitle(getString(R.string.factor_for_customer, vm.customerName))
    }

    private fun setUpFactorItems() {
        rvFactorItems.apply {
            adapter = factorItemAdapter
        }
    }

    private fun setUpProductAdapter() {
        productAdapter.setOnProductClickListener { product ->
            product.selected = !product.selected
            if (product.selected) {
                vm.addProduct(product.copy(selected = false))
                vm.addFactorItem(product.toModel())
                return@setOnProductClickListener
            }
            vm.removeProduct(product.copy(selected = false))
            vm.deleteFactorItem(product)
        }
        binding.rvProducts.apply {
            adapter = productAdapter
        }
    }

    private fun setupStandardBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(viewDialog)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.isDraggable = false
        bottomSheetBehavior.saveFlags = BottomSheetBehavior.SAVE_ALL
    }

    private fun subscribeToObservers() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            vm.productsFlow.collect { it ->
                val products = it.products.map { it.toModel() }
                vm.productUIModels.value = products
            }
        }
        vm.productUIModels.observe(viewLifecycleOwner) {
            productAdapter.submitList(it)
        }
        vm.isFirstShowing.observe(viewLifecycleOwner) {
            if (it) {
                vm.syncProductsWithFactorItems()
                vm.isFirstShowing.value = false
            }
        }
        vm.factorItems.observe(viewLifecycleOwner) { it ->
            val newValue = it.map {
                it.copy(
                    product = it.product,
                    count = it.count,
                    factorId = it.factorId,
                    totalPrice = it.totalPrice,
                    created = it.created,
                    updated = System.currentTimeMillis()
                )
            }
            calculateTotalPrice(it)
            factorItemAdapter.submitList(newValue)
        }
        vm.isAllProductsSelected.observe(viewLifecycleOwner) {
            binding.cbSelectAll.isChecked = it
        }
    }

    private fun calculateTotalPrice(factorItems: List<FactorItem>) {
        var totalPrice = 0f
        factorItems.forEach { factorItem ->
            totalPrice += factorItem.totalPrice
        }
        tvTotalPrice.text = getString(R.string.total_price_value, formatPrice(totalPrice))
    }

    override fun onIncreaseProductCount(factorItem: FactorItem) {
        vm.increaseProductCount(factorItem)
    }

    override fun onDecreaseProductCount(factorItem: FactorItem) {
        vm.decreaseProductCount(factorItem)
    }

    override fun onFactorItemClick(factorItem: FactorItem) {
        val editFactorItemDialog = EditFactorItemDialog(factorItem)
        editFactorItemDialog.addOnUpdateListener {
            vm.onUpdateFactorItem(it)
            editFactorItemDialog.dismiss()
        }
        editFactorItemDialog.show(childFragmentManager, EditFactorItemDialog.TAG)
    }

    override fun onProductPriceChanged(factorItem: FactorItem, price: Int?) {

    }

    override fun onProductCountChanged(factorItem: FactorItem, newCount: Int?) {

    }

    private fun printFactor() {
        requestPermission.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val fileName = "${vm.factorId}-${vm.customerName}-${System.currentTimeMillis()}"
        QuickShot.of(requireView()).setResultListener(this@AddEditFactorFragment)
            .enableLogging()
            .setFilename(fileName)
            .toPNG()
            .save()
    }

    override fun onQuickShotSuccess(path: String?) {
        Timber.i("")
    }

    override fun onQuickShotFailed(path: String?, errorMsg: String?) {
        Timber.i("")
    }

    private fun requestPermission() {
        if (PermissionUtility.hasWriteExternalStoragePermissions(requireContext())) {
            return
        }
        EasyPermissions.requestPermissions(
            this,
            "شما برای ذخیره تصویر این فاکتور باید دسترسی لازم را بدهید",
            REQUEST_CODE_WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {}

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        } else {
            requestPermission()
        }
    }
}