package ir.vbile.app.hesabit.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.navigation.findNavController
import ir.vbile.app.hesabit.R
import ir.vbile.app.hesabit.extentions.margin
import ir.vbile.app.hesabit.extentions.setVisibleOrGone
import kotlinx.android.synthetic.main.view_toolbar.view.*
import timber.log.Timber

class BaseToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.view_toolbar, this)
//        toolbarTitle = tvTitle
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.BaseToolbar)
            val title =
                a.getString(R.styleable.BaseToolbar_ta_title)
            val shouldShowBackBtn =
                a.getBoolean(R.styleable.BaseToolbar_ta_shouldShowBackBtn, false)
            if (title != null) {
                tvTitle.text = title
            }
            if (shouldShowBackBtn) {
                tvTitle.margin(left = 80f,right = 80f)
            } else {
                tvTitle.margin(left = 0f,right = 0f)
            }
            btnBack.setVisibleOrGone(shouldShowBackBtn)
            btnBack.setOnClickListener {
                findNavController().popBackStack()
            }
            a.recycle()
        }
    }

    fun showBackBtn(shouldShow: Boolean, function: () -> Unit? = {}) {
        Timber.i(shouldShow.toString())
        btnBack.setVisibleOrGone(shouldShow)
        btnBack.setOnClickListener {
            function.invoke()
        }
    }

    fun setTitle(title: String) {
        tvTitle.text = title
    }
}