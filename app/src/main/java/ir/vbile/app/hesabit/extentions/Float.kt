package ir.vbile.app.hesabit.extentions

import java.text.NumberFormat

val Float.commaString: String
    get() = NumberFormat.getInstance().format(this)