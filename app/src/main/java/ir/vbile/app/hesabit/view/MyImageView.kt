package ir.vbile.app.hesabit.view

import android.content.Context
import android.util.AttributeSet
import com.facebook.drawee.view.SimpleDraweeView

class MyImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : SimpleDraweeView(context, attrs, defStyleAttr) {
}